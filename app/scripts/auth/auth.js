'use strict';
angular.module('OM.Auth', [])

.factory('AuthFactory', [
  '$firebaseSimpleLogin',
  'FIREBASE_URL',

  function($firebaseSimpleLogin, FIREBASE_URL) {
    console.log('AUTH FACTORY IS RUNNING NOW!');
    var authObj = $firebaseSimpleLogin(new Firebase(FIREBASE_URL));
    
    var Auth = {
      register: function (user) {
        authObj.$logout();
        return authObj.$createUser(user.email, user.password);
      },

      firebaseSignedIn: function() {
        return authObj.$getCurrentUser();
      },


      login: function (user) {
        return authObj.$login('password', user);
      },

      logout: function () {
        authObj.$logout();
      }
    };


    return Auth;

  }

]);


