'use strict';
angular.module('OM.Projects', [])
//READ FIREBASE/ANGULARFIRE API https://www.firebase.com/docs/angular/reference.html
.factory('ProjectList', [
  '$firebase',
  '$rootScope',
  'FIREBASE_URL',
  'Project',
  function($firebase, $rootScope, FIREBASE_URL, Project) {

    var ref = new Firebase(FIREBASE_URL + '/projectlist');
    var projectlist = $firebase(ref);


    var ProjectList = {
      all: projectlist,


      initLastOpened: function() {
        if ($rootScope.currentUser) {
          console.log('setting lastOpened to null');
          return $rootScope.currentUser.$update({lastOpened : null});
        }
      },

      setLastOpened: function(projectId) {
        return $rootScope.currentUser.$update({lastOpened: projectId});
      },

//creates a project item firebase entry at /projectlist with initial data 
//(project title & client)
      create: function(initData) {
        return projectlist.$add(initData)

//updates entry with the firebase-generated ID as an explicit property  
        .then(function(ref){
          $firebase(ref).$update({projectId: ref.name()})
          return ref;
        })

//stores the id of the Last Opened project, for quick reference 
//it's handy when there's a page refresh
        .then(function(ref){
          ProjectList.setLastOpened(ref.name())
          return ref;
        })

//calls Project.create passing the acquired reference & initial data
//to create a firebase entry under /projects
        .then(function(ref){
          Project.create(ref, initData);
          return ref;
        })
      },

      open: function(projectId) {
        return Project.getProjectData(projectId);
      },


//updates /projectlist firebase entry, with changes that occured
//in the  /projects entry for consistency (check Step1Ctrl)
      update: function(projectId, property, value){
        var ref = new Firebase(FIREBASE_URL + '/projectlist/' + projectId);
        var projectToUpdate = $firebase(ref);
        projectToUpdate[property] = value;
        projectToUpdate.$save();
      },

      removeProjectItem: function(projectId) {
        //console.log('you want to delete', projectId);
        return projectlist.$remove(projectId);
      },

      removeProjectData: function(projectId) {
        Project.delete(projectId);
      }
    };

    return ProjectList;

  }

])


.factory('Project', [
  '$rootScope',
  '$firebase',
  'FIREBASE_URL',
  function($rootScope, $firebase, FIREBASE_URL){

    var Project = {

      create: function(ref, initData) {
        var projectref = new Firebase(FIREBASE_URL + '/projects/' + ref.name())
        $firebase(projectref).$update(initData)
      },

      delete: function(projectId) {
        //console.log('deleting the following project', projectId);
        var ref = new Firebase(FIREBASE_URL + '/projects/');
        return $firebase(ref).$remove(projectId);

      },

      getProjectData: function(projectId) {
        var ref = new Firebase(FIREBASE_URL + '/projects/' + projectId);
        var project = $firebase(ref);
        return project;
      },

      getCurrentId: function(){
        if ($rootScope.currentUser) {
 //fist check if currentUser exists, to avoid console error on page refresh
          return $rootScope.currentUser.lastOpened;
        }
      }
    };

    return Project;

  }

]);




