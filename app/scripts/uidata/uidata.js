'use strict';
angular.module('OM.UIData', [])

.factory('UIData', [
	'$firebase',
	'$q',
	'$filter',
	'FIREBASE_URL',

	function($firebase, $q, $filter, FIREBASE_URL) {

		var uiDataRef = $firebase(new Firebase(FIREBASE_URL+'/uidata2'));

		var UIData = {
			getDataFrom: function(entry) {

					var deferred = $q.defer();

				  uiDataRef.$child(entry).$on("loaded", function(data){
						deferred.resolve(data)
					})

					return deferred.promise;
			}
		}

		return UIData
	}
])

//$filter('orderByPriority')(uiDataRef.$child('formFactors'));
			