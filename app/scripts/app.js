'use strict';

angular.module('reqsGatheringApp', [
  'OM.Auth',
  'OM.Login',
  'OM.Signup',
  'OM.Navigation',
  'OM.Home',
  'OM.Projects',
  'OM.User',
  'OM.Form',
  'OM.Document',
  'OM.UIData',

  'ngProgress',

  'ui.router',
  'ui.bootstrap',
  'ngCookies',
  'ngResource',
  'ngSanitize',
])

.run([
  '$rootScope',
  '$state',
  '$stateParams',
  '$firebase',
  'FIREBASE_URL',
  'AuthFactory',
  'User',
  'UIData',
  '$filter',
  function (
  $rootScope,
  $state,
  $stateParams,
  $firebase,
  FIREBASE_URL,
  AuthFactory,
  User,
  UIData,
  $filter) {


    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    //  console.log('LAST OPENED: ',$rootScope.currentUser.lastOpened)
    //  console.log('$rootScope.currentUser: ', $rootScope.currentUser);
    //  console.log('$rootScope.signedIn: ', $rootScope.signedIn());



//if user isn't logged-in and the state requires authentication redirect to 'login' 
      if($rootScope.currentUser === undefined && toState.authRequired) {
        AuthFactory.firebaseSignedIn().then(function(user){
          if (!user) {
            event.preventDefault();
            $state.go('login');
          }
        });
      
//if a firebase:Login event takes place at any state, query firebase for additional user data, 
//and setCurrentUser on $rootScope.currentUser
        $rootScope.$on('$firebaseSimpleLogin:login', function (e, authUser) {
          console.log('LOGIN FIRED FROM RUN');
          var ref = new Firebase(FIREBASE_URL + '/users');
          var query = $firebase(ref.startAt(authUser.uid).endAt(authUser.uid));
         
          query.$on('loaded', function() {
            $rootScope.currentUser = User.findByUsername(query.$getIndex()[0]);
            $state.go($state.current, {}, {reload: true});
          });
        });

//if a firebase:Logout event takes place at any state, delete $rootScope.currentUser
        $rootScope.$on('$firebaseSimpleLogin:logout', function() {
          delete $rootScope.currentUser;
        });
      }
    });

  // It's very handy to add references to $state and $stateParams to the $rootScope
  // so that you can access them from any scope within your applications.For example,
  // <li ui-sref-active="active }"> will set the <li> // to active whenever
  // 'contacts.list' or one of its decendents is active.
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

  }

])




.config(function($stateProvider, $urlRouterProvider) {
  
  $stateProvider

  .state('login', {
    url:'/login',
    views: {
      '': { templateUrl : 'scripts/main/main.html' },

      'headerView@login': {
          templateUrl: 'scripts/login/login.html',
          controller: 'LoginCtrl'
      },
    },
    authRequired: false
  })


  .state('signup', {
    url:'/signup',
    views: {
      '': { templateUrl: 'scripts/main/main.html'},
      'content@signup' : {
        templateUrl : 'scripts/signup/signup.html',
        controller: 'SignupCtrl'
      }
    },
    authRequired: false
  })


  .state('home', {
    url: '/home',
    views: {

      '' : {templateUrl: 'scripts/main/main.html'},
      'headerView@home': {
        templateUrl: 'scripts/user/userinfo.html',
        controller: 'LoginCtrl'
      },
      'content@home' : {
        templateUrl: 'scripts/home/home.html',
        controller: 'HomeCtrl'
      }
    },
    authRequired: true
  })

  .state('step1', {
    url: '/step1',
    views: {
      '' : {templateUrl: 'scripts/main/main.html'},

      'headerView@step1' : {
        templateUrl: 'scripts/user/userinfo.html',
        controller: 'LoginCtrl',
      },
      
      'headerNav@step1' : {
        templateUrl : 'scripts/navigation/navigation.html',
        controller: 'NavCtrl'
      },
      'content@step1' : {
        templateUrl: 'scripts/form/step1.html',
        controller: 'Step1Ctrl'
      },
      'projectinfo@step1' : {
        templateUrl: 'scripts/project/projectinfo.html'
      }
    },
    authRequired: true
  })
  

  .state('step2', {
    url: '/step2',
    views: {
      '' : {templateUrl: 'scripts/main/main.html'},

      'headerView@step2' : {
        templateUrl: 'scripts/user/userinfo.html',
        controller: 'LoginCtrl',
      },
      
      'headerNav@step2' : {
        templateUrl : 'scripts/navigation/navigation.html',
        controller: 'NavCtrl'
      },
      'content@step2' : {
        templateUrl: 'scripts/form/step2.html',
        controller: 'Step2Ctrl'
      },
      'projectinfo@step2' : {
        templateUrl: 'scripts/project/projectinfo.html'
      }
    },
    authRequired: true
  })
  

  .state('step3', {
    url: '/step3',
    views: {
      '' : {templateUrl: 'scripts/main/main.html'},

      'headerView@step3' : {
        templateUrl: 'scripts/user/userinfo.html',
        controller: 'LoginCtrl',
      },
      
      'headerNav@step3' : {
        templateUrl : 'scripts/navigation/navigation.html',
        controller: 'NavCtrl'
      },
      'content@step3' : {
        templateUrl: 'scripts/form/step3.html',
        controller: 'Step3Ctrl'
      },
      'projectinfo@step3' : {
        templateUrl: 'scripts/project/projectinfo.html'
      }
    },
    authRequired: true
  })
  
  .state('document', {
    url:'/document',
    views: {
      '': { templateUrl: 'scripts/main/main.html'},
      'content@document' : {
        templateUrl : 'scripts/document/document.html',
        controller: 'DocCtrl'
      }
    },
    authRequired: true
  })


  $urlRouterProvider.otherwise('/home');

})

.constant('FIREBASE_URL', 'https://prototool.firebaseio.com')

.directive('customCheckbox', function() {
  return {
    restrict: 'E',
    replace: true,
    require: '?ngModel',
    scope: {
      ngModel: '=?',
      ngChange: '&'
    },
    transclude: true,

    template:   '<label class="customcheckbox">' +
                    '<input type="checkbox" ng-model="ngModel" ng-change="ngChange()">'+
                    //htmlText for icon gets injected here
                '</label>',

    compile: function(element, attr) {
      var input = element.find('input');
      var htmlText=  '<i ng-class="ngModel ? \''+attr.checked+'\':\''+attr.unchecked+'\'"></i>';

      input.after(htmlText); //injection
    }
  };
});
