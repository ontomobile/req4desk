'user strict';
angular.module('OM.Document', [])

.controller('DocCtrl', function($scope,$state, Project, templateFactory) {
		//console.log('Hello, this is document controller!');
	  $scope.project = Project.getProjectData(Project.getCurrentId());
	  $scope.template = templateFactory();

})

.directive( 'showDoc', function ( $compile ) {
  return {
    scope: true,
    link: function ( scope, element, attrs ) {
      var el;

      attrs.$observe( 'template', function ( tpl ) {
        if ( angular.isDefined( tpl ) ) {
          // compile the provided template against the current scope
          el = $compile( tpl )( scope );

          // stupid way of emptying the element
          //element.html("");

          // add the template content
          element.append( el );
        }
      });
    }
  };
})

.factory( 'templateFactory', function () {
  return function () {
    // obviously would be $http
    var tpl = '<div> <h1>ONTOMOBILE</h1><h2>{{project.client}} - {{project.title}}</h2> <p><h3>Non-disclosure Agreement</h3></p><p>This document contains confidential information and trade secrets belonging to ONTOMOBILE and delivered solely to enable to make an assessment of the offer.The recipient agrees to treat this information as confidential and not to disclose or reproduce, except for those directly responsible for the proper evaluation of the contents thereof, without the express consent and consensus of ONTOMOBILE.</p><p>ONTOMOBILE reserves the right to recover copies of this document when the assessment has been completed.</p></div>';
    return tpl;
  };
});

