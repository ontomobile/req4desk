'use strict';
angular.module('OM.Home', ['firebase'])

.controller('HomeCtrl',[
	'$scope',
	'$state',
	'ProjectList',
  'ngProgress',
  function($scope, $state, ProjectList, ngProgress) {



    $scope.projectlist = ProjectList.all;
    $scope.initProject = {};

    //when at 'home' state, we initialize the lastOpened project to be null
    ProjectList.initLastOpened();


//creates an entry on firebase /projectlist with initial project data (just title/client)
    $scope.newProjectItem = function () {
      ngProgress.height('4px');
      ngProgress.color('#4486F8');
      ngProgress.start()
      ProjectList.create($scope.initProject).then(function(ref) {
        $scope.open(ref.name());
      });
    };

//converts client string to uppercase
    $scope.uppercase = function() {
      if ($scope.initProject.client) {
        $scope.initProject.client = $scope.initProject.client.toUpperCase();
      }
    }

    $scope.open =function(projectId){
      ngProgress.height('4px');
      ngProgress.color('#4486F8');
      ngProgress.set(80);
      ProjectList.setLastOpened(projectId).then(function(){
        ngProgress.complete();
        $state.go('step1')
      })
    };

    $scope.deleteProject = function (projectId) {
      ProjectList.removeProjectItem(projectId).then(function() {
        return ProjectList.removeProjectData(projectId)
      }).then(function() {
        console.log('Item and data removed successfully!')
      })
    };

  }
]);

