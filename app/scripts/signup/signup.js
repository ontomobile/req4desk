'use strict';
angular.module('OM.Signup', [])

.controller('SignupCtrl', [
  '$scope',
  '$state',
  'AuthFactory',
  'User',
  function($scope, $state, AuthFactory, User) {

    $scope.$on('$firebaseSimpleLogin:login', function () {
      console.log('firebase login event, fired from signup controller');
      $state.go('home');
    });

    $scope.register = function() {
      AuthFactory.register($scope.user).then(function(authUser) {
        User.create(authUser, $scope.user.username).then(function(){
           $state.go('home');
        })
      }, function (error) {
        $scope.error = error.toString();
      });
    };
  }
]);

