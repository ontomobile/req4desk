'use strict';
angular.module('OM.Login', [])
  

.controller('LoginCtrl', [
  '$scope',
  '$state',
  'AuthFactory',
  'ngProgress',
  function($scope, $state, AuthFactory, ngProgress){

//Login ctrl is also included in the topbar, so we have to guard
//against other states, otherwise it will always redirect to 'home'
    AuthFactory.firebaseSignedIn().then(function(user){
      if(user && $state.current.name==="login") {
        console.log('you are already logged in!', $state);
        $state.go('home');
      }
    })

    $scope.login = function() {
      ngProgress.height('4px');
      ngProgress.color('#4486F8');
      ngProgress.start();
      AuthFactory.login($scope.user).then(function(authUser) {
        ngProgress.complete();
        $state.go('home');
      },
      function(error) {
        console.log('login error', error);
        $scope.error = error.toString();
      });
    };


    $scope.logout = function() {
      AuthFactory.logout();
      $state.go('login');
    };
    
    $scope.signup = function() {
      $state.go('signup');
    };



  }
]);

