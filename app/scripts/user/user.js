'use strict';
angular.module('OM.User', [])
 
.factory('User',[
  '$rootScope',
  '$firebase',
  'FIREBASE_URL',
  function ($rootScope, $firebase, FIREBASE_URL) {
    console.log('USER FACTORY IS RUNNING NOW!');
    var ref = new Firebase(FIREBASE_URL + '/users');
    var users = $firebase(ref);

    function setCurrentUser (username) {
      $rootScope.currentUser = User.findByUsername(username);
    }

    $rootScope.$on('$firebaseSimpleLogin:login', function (e, authUser) {
      var query = $firebase(ref.startAt(authUser.uid).endAt(authUser.uid));
       
      query.$on('loaded', function () {
        setCurrentUser(query.$getIndex()[0]);
      });
    });

 // $rootScope.$on('$firebaseSimpleLogin:logout', function() {
 //     delete $rootScope.currentUser;
 //   });



    var User = {
      create: function (authUser, username) {
        users[username] = {
          md5_hash: authUser.md5_hash,
          username: username,
          email: authUser.email,
          lastOpened: '',
          $priority: authUser.uid
        };

       return users.$save().then(function () {
          setCurrentUser(username);
        });
      },

      findByUsername: function (username) {
        if (username) {
          return users.$child(username);
        }
      },

      getCurrent: function () {
        return $rootScope.currentUser;
      },

      lastOpened : function() {
        return $rootScope.currentUser.lastOpened;
      },

      signedIn: function () {
        return $rootScope.currentUser !== undefined;
      }
    };

    return User;

  }

]);