'use strict';
angular.module('OM.Navigation', [])

.controller('NavCtrl',[
  '$scope',
  'Project',
  function($scope, Project) {
    $scope.project = Project.getProjectData(Project.getCurrentId());
	}
]);

