'user strict';
angular.module('OM.Form', [])

.controller('Step1Ctrl', function($scope,$state, Project, ProjectList) {
  $scope.project = Project.getProjectData(Project.getCurrentId());


//mediates title/client changes from a /projects entry to the corresponding /projectlist entry
  $scope.project.$on('loaded', function() {
    $scope.project.$child('title').$on('change', function() {
      ProjectList.update(Project.getCurrentId(), 'title', $scope.project.title);
    });
    $scope.project.$child('client').$on('change', function(){
      ProjectList.update(Project.getCurrentId(), 'client', $scope.project.client);
    });
  });

  $scope.persist = function() {
    $scope.project.$save();
  };

  //converts client string to uppercase
    $scope.uppercase = function() {
      if ($scope.project.client) {
        $scope.project.client = $scope.project.client.toUpperCase();
      }
      
    }
})




.controller('Step2Ctrl', function($scope,$state,$filter,UIData, Project) {
  $scope.project = Project.getProjectData(Project.getCurrentId());

//populate UI using UIData service
  UIData.getDataFrom('formFactors').then(function(formFactorsData){
    $scope.uiFormFactors = formFactorsData;
  })
  .then(function(){
    return UIData.getDataFrom('platforms');
  })
  .then(function(platformsData) {
    for (var index in $scope.uiFormFactors) {
      $scope.uiFormFactors[index].uiPlatforms = angular.copy(platformsData);//angular.copy helps separating uiPlatforms on nested ng-repeat
    }
  })
  .then(function(){
    $scope.project.$on('loaded', function(){
      var specs = $scope.project.$child('specs');
      var targets = specs.$child('targets');


      //CHANGES FROM FIREBASE TO UI HERE
      targets.$on('child_added', function(targetChild){
        $scope.uiFormFactors[targets[targetChild.snapshot.name].formFactor].isSelected = true;
        var platforms = targets.$child(targetChild.snapshot.name).$child('platforms');

        platforms.$on('child_added', function(platformChild){
          $scope.uiFormFactors[targets[targetChild.snapshot.name].formFactor].uiPlatforms[platformChild.snapshot.value.name].isSelected = true;
           
          var concretePlatform = platforms.$child(platformChild.snapshot.name);
          concretePlatform.$on('child_added', function(concretePlatformChild) {
            if (concretePlatformChild.snapshot.name === 'minVersion') {
              $scope.uiFormFactors[targets[targetChild.snapshot.name].formFactor].uiPlatforms[platformChild.snapshot.value.name].minVersion = concretePlatformChild.snapshot.value;
            }
          });

          var minVersion = concretePlatform.$child('minVersion');
          minVersion.$on('change', function() {
            if (concretePlatform.minVersion !== undefined) {
              $scope.uiFormFactors[targets[targetChild.snapshot.name].formFactor].uiPlatforms[platformChild.snapshot.value.name].minVersion = concretePlatform.minVersion;
            }
          });
        });

        platforms.$on('child_removed', function(platformChild){
          //guard for when removing of a platform parent (target)
          if (targets[targetChild.snapshot.name]) {
            $scope.uiFormFactors[targets[targetChild.snapshot.name].formFactor].uiPlatforms[platformChild.snapshot.value.name].isSelected = false;
            $scope.uiFormFactors[targets[targetChild.snapshot.name].formFactor].uiPlatforms[platformChild.snapshot.value.name].minVersion = '';
          }
        });
      });


      targets.$on('child_removed', function(targetChild){
        // console.log('child REMOVED from targets', targetChild.snapshot.value);
        $scope.uiFormFactors[targetChild.snapshot.value.formFactor].isSelected = false;

          //takes care of removing any user selections from the uiPlatforms object, when the formFactor is deselected
          //to avoid breaking the UI with inconsistencies and old selections. 
        for (var uiPlatformKeys in $scope.uiFormFactors[targetChild.snapshot.value.formFactor].uiPlatforms) {
          $scope.uiFormFactors[targetChild.snapshot.value.formFactor].uiPlatforms[uiPlatformKeys].isSelected = false;
          $scope.uiFormFactors[targetChild.snapshot.value.formFactor].uiPlatforms[uiPlatformKeys].minVersion = '';
        }
      });
    });

  });



  $scope.updateFormFactor = function(uiFormFactor) {
    var targets = $scope.project.$child('specs').$child('targets');
    if (uiFormFactor.isSelected) {
      targets.$add({'formFactor': uiFormFactor.name});
    } else {
      for (var targetKeys in targets) {
        if (targets[targetKeys].formFactor === uiFormFactor.name) {
          targets.$remove(targetKeys);
        }
      }
    }
  };

  $scope.updatePlatform = function(uiFormFactor, uiPlatform) {
    var targets = $scope.project.$child('specs').$child('targets');
    //this is a bug fix, isSelected can't be initialized properly.
    //the bug is custom-checkbox directive! Using input type="checkbox"
    //everything works fine WITHOUT the need to perform the inversion below
    uiPlatform.isSelected = !uiPlatform.isSelected;

    for (var targetKeys in targets) {
      if (targets[targetKeys].formFactor === uiFormFactor.name) {
        var platforms = targets.$child(targetKeys).$child('platforms');
        if (uiPlatform.isSelected) {
          platforms.$add({'name' : uiPlatform.name});
        } else {
          for (var platformKeys in platforms) {
            if(platforms[platformKeys].name === uiPlatform.name) {
              platforms.$remove(platformKeys);
            }
          }
        }
      }
    }
  };


  $scope.updateVersion = function(uiFormFactor, uiPlatform) {
    var targets = $scope.project.$child('specs').$child('targets');

    for (var targetKeys in targets) {
      if (targets[targetKeys].formFactor === uiFormFactor.name) {
        var platforms = targets.$child(targetKeys).$child('platforms');
        for (var platformKeys in platforms) {
          if (platforms[platformKeys].name===uiPlatform.name) {
            platforms[platformKeys].minVersion = uiPlatform.minVersion;
            platforms.$save();
          }
        }
      }
    }
  };


  $scope.test = function() {
    console.log('$scope.uiFormFactors ',$scope.uiFormFactors);
  };
})





.controller('Step3Ctrl', function($scope, $state, $filter, Project) {

  $scope.today = function() {
    ///scope.dt has the date in a nice format!
    $scope.dt =  $filter('date')(new Date(),'fullDate');
  };
  $scope.today();

  $scope.clear = function () {
    $scope.dt = null;
  };

  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  };

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };


  $scope.genDoc = function() {
    console.log('you clicked on Generate Doc');
    $state.go('document');
  };






});
